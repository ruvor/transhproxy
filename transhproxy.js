const http = require("http");
const fs = require("fs");
const Logger = require("./logger");

// const proxyHostname = "127.0.0.1";
// const proxyPort = 4000;
// const serviceHostname = "192.168.1.39";
// const servicePort = 8080;
// const serviceCookiesFileName = "cookies.txt";

let serviceCookies;
let _serviceCookiesFileName;

function refreshServiceCookies() {
    serviceCookies = fs.readFileSync(_serviceCookiesFileName, { encoding: "utf8" });
}

function tranhproxy(proxyAuthority, serviceAuthority, serviceCookiesFileName) {
    var proxyAuthoritySplit = proxyAuthority.split(":");
    let proxyHostname = proxyAuthoritySplit[0];
    let proxyPort = proxyAuthoritySplit[1];
    var serviceAuthoritySplit = serviceAuthority.split(":");
    let serviceHostname = serviceAuthoritySplit[0];
    let servicePort = serviceAuthoritySplit[1];
    _serviceCookiesFileName = serviceCookiesFileName;

    const logger = new Logger("SuperCoolLogFile");

    const server = http.createServer((appReq, appRes) => {
        try {
            let appReqData = JSON.stringify({
                url: appReq.url,
                headers: appReq.headers,
                method: appReq.method
            });
            logger.log(`Запрос от приложения: ${appReqData}`);

            appRes.setHeader("Access-Control-Allow-Origin", "*");
            appRes.setHeader("Access-Control-Expose-Headers", "Location");
            appRes.setHeader("Cache-Control", "no-cache");
            appRes.setHeader("Expires", "0");
            if (appReq.method == "OPTIONS") {
                appRes.setHeader("Access-Control-Allow-Headers", "Content-Type");
                appRes.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
            }

            let options = {
                hostname: serviceHostname,
                port: servicePort,
                method: appReq.method,
                path: appReq.url,
                headers: appReq.headers
            };
            let serviceReq = http.request(options, (serviceRes) => {
                logger.log(`Статус ответа службы: ${serviceRes.statusCode}`);

                appRes.writeHead(serviceRes.statusCode, serviceRes.statusMessage, serviceRes.headers);
                serviceRes.on("data", (chunk) => {
                    logger.log("Получен и переправлен чанк тела ответа службы");
                    appRes.write(chunk);
                }).on("end", () => {
                    logger.log("Ответ службы полностью прочитан. Отправка ответа приложению");
                    appRes.end();
                });
            });
            serviceReq.setHeader("cookie", serviceCookies);
            serviceReq.setHeader("host", `${serviceHostname}:${servicePort}`); //нужно, потому что serviceReq делалось с headers: appReq.headers

            serviceReq.on("error", (e) => {
                logger.log(`Ошибка запроса к службе: ${e.message}`);
                appRes.statusCode = 500;
                appRes.end();
            });

            appReq.on("data", (chunk) => {
                logger.log("Получен и переправлен чанк тела запроса приложения");
                serviceReq.write(chunk);
            }).on("end", () => {
                logger.log("Запрос приложения полностью прочитан. Отправка запроса службе");
                serviceReq.end(() => {
                    logger.log("Запрос к службе отправлен");
                });
            });
        }
        catch (e) {
            console.error(e);
        }
    });

    server.listen(proxyPort, proxyHostname, () => {
        console.log(`Прокси слушает на http://${proxyHostname}:${proxyPort}`);
    });

    refreshServiceCookies();
    logger.log(`Куки из файла прочитаны`);

    fs.watch(serviceCookiesFileName, (eventType) => {
        if (eventType != "change") return;
        refreshServiceCookies();
        logger.log(`Куки из файла перепрочитаны`);
    });
}

module.exports = tranhproxy;
