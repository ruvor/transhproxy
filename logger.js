const fs = require("fs");

function Logger(filename) {
    this._filename = filename;
    this.open();
}

Logger.prototype._makeFilename = function () {
    this._dateStr = new Date().toISOString().slice(0, 10);
    return `${this._filename}.${this._dateStr}.log`;
};

Logger.prototype.log = function (data) {
    if (new Date().toISOString().slice(0, 10) != this._dateStr) {
        this.close();
        this.open();
    }
    let str = typeof data == "object" ? JSON.stringify(data) : data;
    this._ws.write(`${new Date().toISOString()}: ${str}\n`);
};

Logger.prototype.open = function () {
    this._ws = fs.createWriteStream(this._makeFilename(), { flags: "a" });
};

Logger.prototype.close = function () {
    this._ws.end();
};

module.exports = Logger;
